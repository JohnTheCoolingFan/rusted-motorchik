This is a rewrite of [Motorchik bot](https://github.com/JohnTheCoolingFan/Motorchik) to rust using [serenity](https://github.com/serenity-rs/serenity) library

Invite link: https://discord.com/api/oauth2/authorize?client_id=551085000775172096&permissions=1426399423606&scope=bot

The code is open source and this is my guarantee why you can safely use the bot with these permissions. There is no arbitrary code execution that is hidden from source code, the bot running on the server is compiled from this exact source.

PLEASE report bugs, imperfections, suggest your feature requests, even the most crazy ones. That would help make a discord bot that doesn't suck and doesn't exploit you. Unlike *some* other bots.
